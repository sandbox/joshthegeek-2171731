# Checkbox Node Access

## This is more development notes than a proper Readme, so you should see the [project page](https://drupal.org/sandbox/joshthegeek/2171731)

### Yet Another Node Access Module™
This module allows you to control access to nodes by role and various other criteria.

### Features:
- Can give certain roles access to the universal access checkbox
- Can give certain roles creation privileges to certain content types, but force someone with global access privileges to give it the okay for everyone to see it
- Can define who 'everyone' is - i.e. can universally block access from anonymous users, ...
- Can set access controls for an entire book

### Access hierarchy:
- Global access
- Per book access
- Per node access

### Permissions
[content-type]: Create public content
: Allows user to create content which is immediately public. Essentially, this is just a default if the user also has '[content-type]: Control own access' (or higher) because the user can change the visibility. If this is off and the user does not have other permissions, a user who does must allow access to the node.
[content-type]: Control own access
: Allows control over visibility of own posts. Overridden by higher settings in the hierarchy above.
[content-type]: Control any access
: Allows control over visibility of any individual post. Overriden by higher settings in the hierarchy above.
Control own book access
: Allows control over an entire book at once. Overridden by higher settings in the hierarchy above.
Control any book access
: Allows control over an entire book at once. Overridden by higher settings in the hierarchy above.
Administer Checkbox Node Access settings
: Allows control over every node at once. This should only be given to trusted users.

Note that the later permissions give access to all previous permissions (with the exception of 'Control own book access', which does not grant '[content-type]: Control all access').

### Structure

