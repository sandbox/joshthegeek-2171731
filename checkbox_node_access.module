<?php
/**
 * @file
 * A node access module that allows simple access control
 */

/**
 * Implements hook_menu().
 */
function checkbox_node_access_menu() {
  $items = array();
  $items['admin/content/checkbox-node-access'] = array(
    'title' => 'Access Control',
    'description' => 'Configuration for Checkbox Node Access module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('checkbox_node_access_config_form'),
    'access arguments' => array('administer checkbox_node_access'),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function checkbox_node_access_permission() {
  $perms = array(
    'cna - administer checkbox_node_access' => array(
      'title' => t('Administer Checkbox Node Access settings'),
      'description' => t('Control universal access.'),
      'restrict access' => TRUE,
    ),
    'cna - control own book access' => array(
      'title' => t('Control own book access'),
    ),
    'cna - control any book access' => array(
      'title' => t('Control any book access'),
    ),
  );

  foreach (node_permissions_get_configured_types() as $type) {
    $perms += _checkbox_node_access_permission_type($type);
  }

  return $perms;
}
/**
 * Helper function to generate standard permission list for a given type.
 *
 * @param $type The machine-readable content type.
 * @return array An array of permission names and descriptions.
 */
function _checkbox_node_access_permission_type($type) {
  $info = node_type_get_type($type);

  // Build standard list of node permissions for this type.
  $perms = array(
    "cna - create public $type content" => array(
      'title' => t('%type_name: Create public content', array('%type_name' => $info->name)),
    ),
    "cna - control own $type access" => array(
      'title' => t('%type_name: Control own access', array('%type_name' => $info->name)),
    ),
    "cna - control any $type access" => array(
      'title' => t('%type_name: Control any access', array('%type_name' => $info->name)),
    ),
  );

  return $perms;
}

/**
 * Page callback: Access Control settings
 * @see checkbox_node_access_menu()
 */
function checkbox_node_access_config_form($form, &$form_state) {
  $form['checkbox_node_access_access_check'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access Check'),
    '#description' => t('Checkbox Access Control only restricts'),
    '#weight' => -50,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => FALSE,
  );
  $form['checkbox_node_access_access_check']['blah'] = array(
    '#type' => 'checkbox',
    '#title' => 'blah',
  );
  $form['#variables_excluded'][] = 'blah';

  $global_visibility_options = array(-1 => t('No access'), 0 => t('Pass through'), 1 => t('All access (possible security risk; use with care)'));
  $form['checkbox_node_access_global_visibility'] = array(
    '#type' => 'radios',
    '#title' => 'Universal node access',
    '#default_value' => variable_get('checkbox_node_access_global_visibility', 0),
    '#options' => $global_visibility_options,
    '#description' => t('No access will block everyone without the bypass permission from viewing content (even the content owner). Pass through allows individual nodes to control access. All access gives everyone access to everything.'),
  );

  return checkbox_node_access_system_settings_form($form);
}

/**
 * Replacement for system_settings_form() which sets checkbox_node_access_system_settings_form_submit() as the submit handler.
 */
function checkbox_node_access_system_settings_form($form) {
  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  if (!empty($_POST) && form_get_errors()) {
    drupal_set_message(t('The settings have not been saved because of the errors.'), 'error');
  }
  $form['#submit'][] = 'checkbox_node_access_system_settings_form_submit';
  // By default, render the form using theme_system_settings_form().
  if (!isset($form['#theme'])) {
    $form['#theme'] = 'system_settings_form';
  }
  return $form;
}
/**
 * Replacement for system_settings_form_submit() which does not set variables for items with $form['#variables_excluded'][$element_name] set to TRUE.
 */
function checkbox_node_access_system_settings_form_submit($form, &$form_state) {
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $key => $value) {
    if ($form['#variables_excluded'] && in_array($key, $form['#variables_excluded'])) continue;

    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    variable_set($key, $value);
  }

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for node_form().
 */
function checkbox_node_access_form_node_form_alter(&$form, &$form_state, $form_id) {
  $form['checkbox_node_access'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access control settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE, //empty($path['alias']),
    '#group' => 'additional_settings',
    '#attributes' => array(
      'class' => array('checkbox-node-access-form'),
    ),
    '#access' => TRUE, //user_access('administer checkbox_node_access'),
    '#weight' => 29,
    '#tree' => TRUE,
    '#description' => t('Checkbox Node Access settings'),
    // '#element_validate' => array('path_form_element_validate'),
  );
  $form['checkbox_node_access']['node_access'] = array(
    '#type' => 'checkbox',
    '#title' => t('Node access'),
    '#description' => t('Whether '),
    '#weight' => 0,
  );
  $form['checkbox_node_access']['book_access'] = array(
    '#type' => 'checkbox',
    '#title' => t('Apply to entire book'),
    '#description' => t('Whether this book (including all subpages) is public. This overrides the "Node access" setting.'),
    '#weight' => 1,
    '#default_value' => TRUE,
    '#access' => (user_access('cna - control own book access') || user_access('cna - control any book access')) && TRUE, //$is_book,
  );
}

/**
 * Implements hook_node_access().
 */
function checkbox_node_access_node_access($node, $op, $account) {
  $global_visibility = variable_get('checkbox_node_access_global_visibility', 0);
  if ($global_visibility == 1) return NODE_ACCESS_ALLOW;
  else if ($global_visibility == -1) return NODE_ACCESS_DENY;

  if (is_string($node)) return NODE_ACCESS_IGNORE;
  $type = $node->type;
  if (!in_array($type, node_permissions_get_configured_types())) return NODE_ACCESS_IGNORE;

  if ($op == 'create' || $op == 'delete' || $op == 'update' || $op == 'view') {

  }

  return NODE_ACCESS_IGNORE;
}

/**
 * Implements hook_node_load().
 */
function checkbox_node_access_node_load($nodes, $types) {

}
